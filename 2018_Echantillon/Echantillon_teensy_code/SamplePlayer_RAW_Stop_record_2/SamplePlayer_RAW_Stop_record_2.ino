#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <Bounce2.h> // to have a array of bounces

//Define the pins used for the sd card reader

#define SDCARD_CS_PIN   (10)
#define SDCARD_MOSI_PIN (7)
#define SDCARD_SCK_PIN  (14)

// Remember which mode we're doing
enum {stopped,recording,playing}; 
int mode = stopped;  // 0=stopped, 1=recording, 2=playing

AudioInputI2S            i2s2;           //xy=105,63
AudioAnalyzePeak         peak1;          //xy=278,108
AudioRecordQueue         queue1;         //xy=281,63
AudioPlaySdRaw           playRaw1;       //xy=302,157
AudioOutputI2S           i2s1;           //xy=470,120

AudioConnection          patchCordA(i2s2, 0, queue1, 0);
AudioConnection          patchCordB(i2s2, 0, peak1, 0);
AudioConnection          patchCordC(playRaw1, 0, i2s1, 0);
AudioConnection          patchCordD(playRaw1, 0, i2s1, 1);
AudioControlSGTL5000     audioShield;    //xy=341,471

const int nbRecordedSounds = 2;
const byte nbButtons = 5;
const byte debounceDelay = 15;
Bounce button[nbButtons];

const int myInput = AUDIO_INPUT_MIC;

// The file where data is recorded
File frec;

void setup() {
  Serial.begin(115200);
  while(!Serial);

  AudioMemory(100);

  for (int i = 0 ; i < nbButtons ; i++){ //debounce your buttons!
    button[i] = Bounce(i, debounceDelay);
    pinMode(i, INPUT_PULLUP);
    //Serial.print("init: ");
    //Serial.println(i);
  }
  
  // Setup SPI in setup
  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);
  if (!(SD.begin(SDCARD_CS_PIN))) {
    // stop here, but print a message repetitively
    while (1) {
      Serial.println("Unable to access the SD card");
      delay(500);
    }
  }
  // Audio connections require memory to work.  For more
  // detailed information, see the MemoryAndCpuUsage example
  
  // turn on the output
  audioShield.enable();
  audioShield.inputSelect(myInput);
  audioShield.volume(0.5);

  delay(500);
  Serial.println("starting");  
}

const char filenameTemplate[] = "RECORD-%d.RAW";
const int buffSize = sizeof(filenameTemplate);  // assumes AT MOST 2 digits for file number!!
char* getFileName(int i){
  // inside a function, if you declare a static variable, it retains its value between for ex
  // if I say:
  // void f(){
  //   static int x =0;
  //   x = x +1;
  //   Serial.println(x);
  // }
  // at the first call it prints :  1
  // at the second call it prints : 2
  static char buff[buffSize];
  sprintf (buff, filenameTemplate, i);  // here we insert the counter value into the filename template
  return buff;                          // now we return the adress of the filled in filename 
}

void startRecording(int i) {
  char* filename = getFileName(i);
  if (SD.exists(filename)) {
    // The SD library writes new data to the end of the
    // file, so to start a new recording, the old file
    // must be deleted before new data is written.
    SD.remove(filename);
  }
  frec = SD.open(filename, FILE_WRITE);
  if (frec) {
    queue1.begin();
    mode = recording;
  }
  Serial.println("startRecording");
}

void continueRecording() {
  if (queue1.available() >= 2) {
    byte buffer[512];
    // Fetch 2 blocks from the audio library and copy
    // into a 512 byte buffer.  The Arduino SD library
    // is most efficient when full 512 byte sector size
    // writes are used.
    memcpy(buffer, queue1.readBuffer(), 256);
    queue1.freeBuffer();
    memcpy(buffer+256, queue1.readBuffer(), 256);
    queue1.freeBuffer();
    // write all 512 bytes to the SD card
    // elapsedMicros usec = 0;
    frec.write(buffer, 512);
    // Uncomment these lines to see how long SD writes
    // are taking.  A pair of audio blocks arrives every
    // 5802 microseconds, so hopefully most of the writes
    // take well under 5802 us.  Some will take more, as
    // the SD library also must write to the FAT tables
    // and the SD card controller manages media erase and
    // wear leveling.  The queue1 object can buffer
    // approximately 301700 us of audio, to allow time
    // for occasional high SD card latency, as long as
    // the average write time is under 5802 us.
    //Serial.print("SD write, us=");
    //Serial.println(usec);
  }
}

void stopRecording() {
  Serial.println("stopRecording");
  queue1.end();
  if (mode == recording) {
    while (queue1.available() > 0) {
      frec.write((byte*)queue1.readBuffer(), 256);
      queue1.freeBuffer();
    }
    frec.close();
  }
  mode = stopped;
}

void startPlaying(int i) {
  Serial.println("startPlaying");
  char* filename = getFileName(i);
  playRaw1.play(filename);
  mode = playing;
}

void continuePlaying() {
  if (!playRaw1.isPlaying()) {
    playRaw1.stop();
    mode = stopped;
  }
}

void stopPlaying() {
  Serial.println("stopPlaying");
  if (mode == stopped) playRaw1.stop();
  mode = stopped;
}

void adjustMicLevel() {
  // TODO: read the peak1 object and adjust sgtl5000_1.micGain()
  // if anyone gets this working, please submit a github pull request :-)
}

void loop() {
  for (int i = 0 ; i < nbButtons ; i++){ //update your buttons!
    button[i].update(); 
  }

  for (int i = 0 ; i < nbButtons ; i++){ //count your buttons!
    switch(i){
      // zero and one are recording
      case 0:
      case 1:
        if (button[i].fallingEdge()) {
          Serial.print("Button ");
          Serial.print(i);
          Serial.println(" is pressed");
          if (mode == playing) 
            stopPlaying();
          else if (mode == stopped) 
           startRecording(i);
        }
        break;
     // case 2 and 3, playback
     case 2:
     case 3:
        if (button[i].fallingEdge()) {
          Serial.print("Button ");
          Serial.print(i);
          Serial.println(" is pressed");
          if (mode == recording) stopRecording();
          startPlaying(i-nbRecordedSounds);
         }
        break;
    // case 4: stop everything
     case 4:
        if (button[i].fallingEdge()) {
          Serial.print("Button ");
          Serial.print(i);
          Serial.println(" is pressed");
          if (mode == recording) stopRecording();
        }
        break;
     // not implemented
     default:
        break;
    }
  }
  // If we're playing or recording, carry on...
  if (mode == recording) {
    continueRecording();
  }
  if (mode == playing) {
    continuePlaying();
  }

  // when using a microphone, continuously adjust gain
  if (myInput == AUDIO_INPUT_MIC) adjustMicLevel();

  /*Serial.print("Max CPU Usage = "); //Monitor max cpu usage
  Serial.print(AudioProcessorUsageMax(), 1);
  Serial.println("%");*/
}


