Bela projects using the MPR121 Adafruit Capacitive Touch Sensor.

- To know more about i2c1 and 2 in Bela.
  https://forum.bela.io/d/593-mpr121-example-touch-failed-to-write-register
- Connecting Bela Mini with MPR121 - do's and don'ts in the render.cpp library
  https://forum.bela.io/d/509-mpr121-capacitive-touch-and-pure-data.
- Bela IDE - commandline. Uploading arrays to Bela and using Touch to play and record them.
  https://forum.bela.io/d/597-eternal-discconnect-initializing-disconnect
