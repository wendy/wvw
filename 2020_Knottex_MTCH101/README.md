## The story

What happens when you have been working with tactile textile and touch in this era, where transmission of viral particles is a hot topic? Working with the MPR121 board? You stop and think it over. During the hard lockdown in May I started to think about tactility and proximity, activating without touching. I could not imagine a public touching the installation I was building. I researched already existing boards and possibilities and I didn’t find what I wanted. I dove into the realms of all kinds of chips and circuits. Infrared, ultrasonic, time of flight – they are all sensors that handle proximity and distance but they do not turn your conductive textile into a tactile interactive object. My second find was bingo: the MTCH101 chip, is a switch, that means it turns something on and off, it makes your conductive textile into a button by hovering over it. In the words of the company that makes them: the MTCH101 is a low cost single channel capacitive proximity detector. The distance of activation and the sensitivity depends on the voltage you give it and the surface of your conductive antenna. The activation can even happen with a non conductive layer covering it. If the switch has a temporary glitch, it recalibrates itself very fluently.

This is the first version, with through hole components. 

## Materials

- PCB
- soldered components
- conductive textile antenna
- integrated in a way that you can easily hook it up to a microcontroller

## Credits

Designed by Wendy Van Wynsberghe with technical assistance of VAEngineering for Knottex, Timelab.
Cern Open Hardware License

**Images** are here: http://etextile-summercamp.org/swatch-exchange/sewable-proximity-switch-board-1/
