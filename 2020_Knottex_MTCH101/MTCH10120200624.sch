EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "MTCH101Touch Proximity Sensor Switch"
Date "2020-06-22"
Rev "v01"
Comp ""
Comment1 "For Knottex - Timelab"
Comment2 "CERN-OHL"
Comment3 "License: CERN Open Hardware License "
Comment4 "Author: Wendy Van Wynsberghe & Thomas Van Aken"
$EndDescr
$Comp
L Device:R R3
U 1 1 5EECE401
P 5450 2700
F 0 "R3" H 5520 2746 50  0000 L CNN
F 1 "4.7K" H 5520 2655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5380 2700 50  0001 C CNN
F 3 "~" H 5450 2700 50  0001 C CNN
	1    5450 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5EED0728
P 5550 3150
F 0 "#PWR03" H 5550 2900 50  0001 C CNN
F 1 "GND" H 5555 2977 50  0000 C CNN
F 2 "" H 5550 3150 50  0001 C CNN
F 3 "" H 5550 3150 50  0001 C CNN
	1    5550 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3150 5600 3150
$Comp
L Device:C C1
U 1 1 5EEEBBF2
P 7300 3300
F 0 "C1" H 7415 3346 50  0000 L CNN
F 1 "0.1µF" H 7415 3255 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D6.0mm_W4.4mm_P5.00mm" H 7338 3150 50  0001 C CNN
F 3 "~" H 7300 3300 50  0001 C CNN
	1    7300 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5EEECB88
P 7300 3550
F 0 "#PWR05" H 7300 3300 50  0001 C CNN
F 1 "GND" H 7305 3377 50  0000 C CNN
F 2 "" H 7300 3550 50  0001 C CNN
F 3 "" H 7300 3550 50  0001 C CNN
	1    7300 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5EEEF536
P 5100 3700
F 0 "R2" H 5170 3746 50  0000 L CNN
F 1 "10K" H 5170 3655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5030 3700 50  0001 C CNN
F 3 "~" H 5100 3700 50  0001 C CNN
	1    5100 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5EEF01B8
P 5400 3450
F 0 "R4" H 5470 3496 50  0000 L CNN
F 1 "10K" H 5470 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5330 3450 50  0001 C CNN
F 3 "~" H 5400 3450 50  0001 C CNN
	1    5400 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5EEF048D
P 5100 3200
F 0 "R1" H 5170 3246 50  0000 L CNN
F 1 "10K" H 5170 3155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5030 3200 50  0001 C CNN
F 3 "~" H 5100 3200 50  0001 C CNN
	1    5100 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5EEF14DA
P 7900 4100
F 0 "R6" H 7970 4146 50  0000 L CNN
F 1 "1K" H 7970 4055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7830 4100 50  0001 C CNN
F 3 "~" H 7900 4100 50  0001 C CNN
	1    7900 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5EF0BCE1
P 5100 3950
F 0 "#PWR02" H 5100 3700 50  0001 C CNN
F 1 "GND" H 5105 3777 50  0000 C CNN
F 2 "" H 5100 3950 50  0001 C CNN
F 3 "" H 5100 3950 50  0001 C CNN
	1    5100 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5EF1344E
P 8150 4100
F 0 "R7" H 8220 4146 50  0000 L CNN
F 1 "4.7K" H 8220 4055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8080 4100 50  0001 C CNN
F 3 "~" H 8150 4100 50  0001 C CNN
	1    8150 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2850 5600 2850
Wire Wire Line
	5100 2950 5100 3050
Wire Wire Line
	5100 3350 5100 3450
Wire Wire Line
	5100 3850 5100 3950
Wire Wire Line
	5250 3450 5100 3450
Connection ~ 5100 3450
Wire Wire Line
	5100 3450 5100 3550
Wire Wire Line
	5550 3450 5600 3450
Wire Wire Line
	6800 3150 7300 3150
Connection ~ 7300 3150
Wire Wire Line
	7300 3450 7300 3550
$Comp
L Device:LED D1
U 1 1 5EF52969
P 7900 3600
F 0 "D1" V 7847 3680 50  0000 L CNN
F 1 "LED" V 7938 3680 50  0000 L CNN
F 2 "LED_THT:LED_D3.0mm" H 7900 3600 50  0001 C CNN
F 3 "~" H 7900 3600 50  0001 C CNN
	1    7900 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 3450 7000 3450
Wire Wire Line
	7000 3450 7000 4250
Wire Wire Line
	7000 4250 7250 4250
Wire Wire Line
	8150 4250 7900 4250
Connection ~ 7900 4250
Wire Wire Line
	7900 3950 7900 3750
Text GLabel 7900 3300 1    50   Input ~ 0
3.3VoltBela
Text GLabel 8150 3300 1    50   Input ~ 0
3.3VoltBela
Wire Wire Line
	7900 3450 7900 3300
Wire Wire Line
	8150 3950 8150 3300
Text GLabel 9300 3250 0    50   Input ~ 0
3.3VoltBela
Wire Wire Line
	7300 3050 7300 3150
Text GLabel 7300 3050 1    50   Input ~ 0
3.3VoltBela
$Comp
L MTCH101-PROXIMITY:MTCH101-PROXIMITY U1
U 1 1 5EECD188
P 7400 3650
F 0 "U1" H 6200 4817 50  0000 C CNN
F 1 "MTCH101-PROXIMITY" H 6200 4726 50  0000 C CNN
F 2 "2020_Kicad:SOT23" H 7400 3650 50  0001 L BNN
F 3 "" H 7400 3650 50  0001 C CNN
	1    7400 3650
	1    0    0    -1  
$EndComp
Text GLabel 5100 2950 1    50   Input ~ 0
3.3VoltBela
Text GLabel 7150 2600 1    50   Input ~ 0
3.3VoltBela
Wire Wire Line
	7150 2600 7150 2850
Wire Wire Line
	7150 2850 7100 2850
$Comp
L Device:R R5
U 1 1 5EEF1130
P 6950 2850
F 0 "R5" H 7020 2896 50  0000 L CNN
F 1 "10K" H 7020 2805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6880 2850 50  0001 C CNN
F 3 "~" H 6950 2850 50  0001 C CNN
	1    6950 2850
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Male 3.3V1
U 1 1 5F2D1A19
P 9600 3450
F 0 "3.3V1" V 9754 3362 50  0000 R CNN
F 1 "Conn_01x01_Male" V 9663 3362 50  0001 R CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x01_P2.00mm_Vertical" H 9600 3450 50  0001 C CNN
F 3 "~" H 9600 3450 50  0001 C CNN
	1    9600 3450
	0    1    -1   0   
$EndComp
Wire Wire Line
	9600 3850 9600 3950
$Comp
L power:GND #PWR0101
U 1 1 5F2C9D00
P 9600 3950
F 0 "#PWR0101" H 9600 3700 50  0001 C CNN
F 1 "GND" H 9605 3777 50  0000 C CNN
F 2 "" H 9600 3950 50  0001 C CNN
F 3 "" H 9600 3950 50  0001 C CNN
	1    9600 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male GND1
U 1 1 5F2C4CCE
P 9600 3650
F 0 "GND1" V 9754 3562 50  0000 R CNN
F 1 "Conn_01x01_Male" V 9663 3562 50  0001 R CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x01_P2.00mm_Vertical" H 9600 3650 50  0001 C CNN
F 3 "~" H 9600 3650 50  0001 C CNN
	1    9600 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 2400 5450 2550
$Comp
L Connector:Conn_01x01_Male Antenna1
U 1 1 5F30039D
P 5300 2200
F 0 "Antenna1" V 5454 2112 50  0000 R CNN
F 1 "Conn_01x01_Male" V 5363 2112 50  0001 R CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x01_P2.00mm_Vertical" H 5300 2200 50  0001 C CNN
F 3 "~" H 5300 2200 50  0001 C CNN
	1    5300 2200
	0    1    1    0   
$EndComp
$Comp
L Device:Antenna AE1
U 1 1 5EED10D3
P 5450 2200
F 0 "AE1" H 5530 2189 50  0000 L CNN
F 1 "Antenna" H 5530 2098 50  0000 L CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x01_P2.00mm_Vertical" H 5450 2200 50  0001 C CNN
F 3 "~" H 5450 2200 50  0001 C CNN
	1    5450 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2400 5450 2400
Connection ~ 5450 2400
Wire Wire Line
	9300 3250 9600 3250
$Comp
L Connector:Conn_01x01_Male GpioDig1b1
U 1 1 5F31B747
P 7250 4450
F 0 "GpioDig1b1" V 7404 4362 50  0000 R CNN
F 1 "Conn_01x01_Male" V 7313 4362 50  0001 R CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x01_P2.00mm_Vertical" H 7250 4450 50  0001 C CNN
F 3 "~" H 7250 4450 50  0001 C CNN
	1    7250 4450
	0    1    -1   0   
$EndComp
Connection ~ 7250 4250
Wire Wire Line
	7250 4250 7900 4250
$Comp
L Mechanical:MountingHole H1
U 1 1 5F2AF23A
P 4650 2000
F 0 "H1" H 4750 2046 50  0000 L CNN
F 1 "MountingHole" H 4750 1955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 4650 2000 50  0001 C CNN
F 3 "~" H 4650 2000 50  0001 C CNN
	1    4650 2000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F2B1A97
P 8150 2000
F 0 "H2" H 8250 2046 50  0000 L CNN
F 1 "MountingHole" H 8250 1955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 8150 2000 50  0001 C CNN
F 3 "~" H 8150 2000 50  0001 C CNN
	1    8150 2000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F2B3437
P 8050 4700
F 0 "H3" H 8150 4746 50  0000 L CNN
F 1 "MountingHole" H 8150 4655 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 8050 4700 50  0001 C CNN
F 3 "~" H 8050 4700 50  0001 C CNN
	1    8050 4700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F2B3D4F
P 4600 4650
F 0 "H4" H 4700 4696 50  0000 L CNN
F 1 "MountingHole" H 4700 4605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 4600 4650 50  0001 C CNN
F 3 "~" H 4600 4650 50  0001 C CNN
	1    4600 4650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
